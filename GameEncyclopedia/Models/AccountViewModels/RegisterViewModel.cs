﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using GameEncyclopedia.Resources.AccountResources;
using GameEncyclopedia.Resources.CommonResources;

namespace GameEncyclopedia.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.Required))]
        [StringLength(12, MinimumLength = 6, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessage =nameof(AccountValidationResource.StringLength))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.Required))]
        [EmailAddress(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidEmailAddressFormat))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name ="Email")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Password")]
        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.Required))]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])(?=.*\W).$", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName =nameof(AccountValidationResource.ValidatePassword))]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceType =typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        public string Password { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessage =nameof(AccountValidationResource.PasswordMismatch))]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "BirthDate")]
        public DateTime? BirthDate { get; set; }

        public bool AcceptFirstRule { get; set; }

        public bool AcceptSecondRule { get; set; }
    }
}