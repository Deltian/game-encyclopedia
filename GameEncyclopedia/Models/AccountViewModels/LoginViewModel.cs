﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using GameEncyclopedia.Resources;
using GameEncyclopedia.Resources.AccountResources;
using GameEncyclopedia.Resources.CommonResources;

namespace GameEncyclopedia.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Display(ResourceType =typeof(AccountDisplayNameResource), Name ="Username")]
        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName ="Required")]
        public string Username { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Password")]
        [Required(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = "Required")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "RememberMe")]
        public bool RememberMe { get; set; }
    }
}