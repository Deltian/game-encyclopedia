﻿using GameEncyclopedia.Models.AccountViewModels;
using GameEncyclopedia.Resources.AccountResources;
using GameEncyclopedia.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameEncyclopedia.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: Account
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            bool checkEmailIsTaken = _userService.CheckUserExists(u => u.Email == model.Email);

            if (checkEmailIsTaken)
                ModelState.AddModelError("", AccountValidationResource.EmailIsAlreadyTaken);

            bool checkUsernameIsTaken = _userService.CheckUserExists(u => u.Username == model.Username);

            if (checkUsernameIsTaken)
                ModelState.AddModelError("", AccountValidationResource.UsernameIsAlreadyTaken);

            if (!ModelState.IsValid)
                return View(model);

            _userService.RegisterUser(model);

            return RedirectToAction("Index");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            var returnUrl = HttpContext.Request.UrlReferrer.AbsoluteUri;

            if (!ModelState.IsValid)
            {
                TempData["modelState"] = ModelState;

                TempData["loginModel"] = model;

                return Redirect(returnUrl);
            }

            _userService.LoginUser(model);

            return Redirect(returnUrl);


        }
    }
}