﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;
using GameEncyclopedia.Repositories;

namespace GameEncyclopedia.App_Start
{
    public static class AutomapperConfig
    {
        public static void CreateMappings()
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<RegisterViewModel, User>();
                c.CreateMap<User, FindUserQueryModel>();
            }
            );

            Mapper.AssertConfigurationIsValid();
        }
    }
}