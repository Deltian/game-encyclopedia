﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using GameEncyclopedia.Repositories;
using GameEncyclopedia.Services;
using GameEncyclopedia.Utils;
using NHibernate;
using NHibernate.Cfg;

namespace GameEncyclopedia
{
    public class IocConfigurator
    {
        public static void ConfigureDependencyInjection()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();

            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();

            builder.Register(c => NHibernateConfig.ConfigureNHibernate()).SingleInstance();
            builder.RegisterType<NHibernateHelper>().AsSelf().InstancePerRequest();
            builder.Register((c, p) => NHibernateConfig.InitializeSessionFactory(p.Named<Configuration>("config"))).InstancePerRequest();



            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}