﻿using FluentNHibernate.Cfg;
using GameEncyclopedia.DomainModels;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using Configuration = NHibernate.Cfg.Configuration;

namespace GameEncyclopedia.Utils
{
    public static class NHibernateConfig
    {
        public static Configuration ConfigureNHibernate()
        {
            var cfg = new Configuration();

            cfg.DataBaseIntegration(x => {
                x.ConnectionString = ConfigurationManager.ConnectionStrings["GameEncyclopedia"].ConnectionString;
                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2012Dialect>();
            });

            cfg.AddAssembly(Assembly.GetAssembly(typeof(User)));

            return cfg;
        }

        public static ISessionFactory InitializeSessionFactory(Configuration config)
        {
            return Fluently.Configure(config).BuildSessionFactory();
        }
    }
}