﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameEncyclopedia.Utils
{
    public enum RegistrationStatus
    {
        Success,
        DuplicateEmail,
        Duplicate
    }
}