﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;

namespace GameEncyclopedia.Services
{
    public class NHibernateHelper
    {
        private readonly ISessionFactory _sessionFactory;

        public NHibernateHelper(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public void SendCommand<TDomainModel>(TDomainModel model, Action<ISession, TDomainModel> command)
        {
            using (var session = _sessionFactory.OpenSession())
            {

                using (var transaction = session.BeginTransaction())
                {

                    command(session,model);
                    transaction.Commit();
                }
            }
        }

        public TResult DoQuery<TResult>(Func<ISession, TResult> query)
        {
            using (var session = _sessionFactory.OpenSession())
            {

                using (var transaction = session.BeginTransaction())
                {

                    var results=  query(session);
                    transaction.Commit();

                    return results;
                }
            }
        }
    }
}