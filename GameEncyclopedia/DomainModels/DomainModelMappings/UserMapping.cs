﻿using FluentNHibernate.Mapping;
using GameEncyclopedia.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameEncyclopedia.DomainModels.DomainModelMappings
{
    public class UserMapping : ClassMap<User>
    {
        public UserMapping()
        {
            Table("User");
            Schema("dbo");
            Id(u => u.Id);
            Map(u => u.Username).Not.Nullable();
            Map(u => u.Email).Not.Nullable();
            Map(u => u.PasswordHash).Not.Nullable();
        }
    }
}