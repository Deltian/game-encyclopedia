﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(GameEncyclopedia.Startup))]
namespace GameEncyclopedia
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Home/Index?openLoginDialog=true"),

            });
        }
    }
}