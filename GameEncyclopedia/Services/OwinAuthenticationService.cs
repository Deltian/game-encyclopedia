﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;
using GameEncyclopedia.Repositories;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace GameEncyclopedia.Services
{
    public class OwinAuthenticationService
    {
        private readonly HttpContext _context = HttpContext.Current;
        private const string AuthenticationType = "ApplicationCookie";

        public OwinAuthenticationService()
        {
        }

        public void SignIn(FindUserQueryModel user, bool isPersistent, params string[] roleNames)
        {
            IList<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", user.Id.ToString())
                //new Claim(ClaimTypes.GivenName, user.FirstName),
                //new Claim(ClaimTypes.Surname, user.LastName),
            };

            foreach (string roleName in roleNames)
            {
                claims.Add(new Claim(ClaimTypes.Role, roleName));
            }

            ClaimsIdentity identity = new ClaimsIdentity(claims, AuthenticationType);

            IOwinContext context = _context.Request.GetOwinContext();
            IAuthenticationManager authenticationManager = context.Authentication;

            authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        public void SignOut()
        {
            IOwinContext context = HttpContext.Current.GetOwinContext();
            IAuthenticationManager authenticationManager = context.Authentication;

            authenticationManager.SignOut(AuthenticationType);
        }
    }
}