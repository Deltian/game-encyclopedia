﻿using AutoMapper;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;
using GameEncyclopedia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace GameEncyclopedia.Services
{
    public class UserService : IUserService
    {
        private readonly OwinAuthenticationService _authenticationService = new OwinAuthenticationService();
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }

        private string HashPassword(string password)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                var bytes = System.Text.Encoding.ASCII.GetBytes(password);

                var hashBytes = shaM.ComputeHash(bytes);

                return BitConverter.ToString(hashBytes);
            }
        }

        public bool LoginUser(LoginViewModel model)
        {
            var user = _repository.Find(u => u.Username == model.Username);

            if (user == null)
                return false;

            var passwordHash = HashPassword(model.Password);

            if (user.PasswordHash != passwordHash)
                return false;

            _authenticationService.SignIn(user, model.RememberMe, new[] { "User" });

            return true;
        }

        public void RegisterUser(RegisterViewModel model)
        {
            _repository.Find(u => u.Username == model.Username);

            var user = Mapper.Map<User>(model);

            user.Id = Guid.NewGuid();

            user.PasswordHash = HashPassword(model.Password);

            _repository.Save(user);

            var userQueryModel = Mapper.Map<FindUserQueryModel>(user);

            _authenticationService.SignIn(userQueryModel, false, new[] { "User" });
        }

        public bool CheckUserExists(Func<User, bool> predicate)
        {
            return _repository.CheckUserExists(predicate);
        }
    }
}