﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;

namespace GameEncyclopedia.Services
{
    public interface IUserService
    {
        bool LoginUser(LoginViewModel model);

        void RegisterUser(RegisterViewModel model);

        bool CheckUserExists(Func<User, bool> predicate);
    }
}
