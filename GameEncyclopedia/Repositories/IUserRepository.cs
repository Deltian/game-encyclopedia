﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;

namespace GameEncyclopedia.Repositories
{
    public interface IUserRepository
    {
        void Save(User user);

        FindUserQueryModel Find(Func<User, bool> predicate);

        bool CheckUserExists(Func<User, bool> predicate);
    }
}