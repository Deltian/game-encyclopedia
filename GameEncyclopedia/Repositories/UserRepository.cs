﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameEncyclopedia.DomainModels;
using GameEncyclopedia.Models.AccountViewModels;
using GameEncyclopedia.Services;

namespace GameEncyclopedia.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly NHibernateHelper _nHibernateHelper;

        public UserRepository(NHibernateHelper nHibernateHelper)
        {
            _nHibernateHelper = nHibernateHelper;
        }

        public bool CheckUserExists(Func<User, bool> predicate)
        {
            var userExisits = _nHibernateHelper.DoQuery(s =>
            s.Query<User>().Any(predicate));

            return userExisits;
        }

        public FindUserQueryModel Find(Func<User, bool> predicate)
        {
            var user = _nHibernateHelper.DoQuery(s =>
                s.Query<User>().
                Where(predicate).Select(u => new FindUserQueryModel(u.Id,u.Username, u.PasswordHash)).
                FirstOrDefault());

            return user;
        }

        public void Save(User user)
        {
            _nHibernateHelper.SendCommand(user, (s, m) => s.Save(m));
        }
    }
}