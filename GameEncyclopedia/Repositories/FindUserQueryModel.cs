﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameEncyclopedia.Repositories
{
    public class FindUserQueryModel
    {
        public FindUserQueryModel(Guid id, string username, string passwordHash)
        {
            Id = id;

            Username = username;

            PasswordHash = passwordHash;
        }

        public Guid Id { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }
    }
}